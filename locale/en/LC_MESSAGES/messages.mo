��    �      �  K  ,      X     Y     y     �     �     �     �     �     �     �     �     �     �     �     �  	   �     �  	   �                     %     :     Q     g     }     �     �     �     �     �     �     	          +     9  
   A     L     U     a  �   o  q   �  H   b     �     �     �     �     �     	          -  
   =  #   H     l     �     �     �  "   �     �          &     D     ]  0   y  (   �  "   �     �          '  "   6     Y  "   j  .   �  E   �  @     �   C  1   �  1     O   I  S   �     �     m     v  2   }  1   �     �     �     �          %  1   C  0   u     �     �     �  B   �  2   &  8   Y     �     �  "   �     �  0   �     
            "      0      A      Q       b      �      �      �   5   �   >   �      2!  
   :!  
   E!     P!  	   _!     i!     o!     x!     �!     �!  +   �!     �!     "     "     '"     5"     D"     T"  /   d"     �"     �"  7   �"  @   �"     #  T   #     t#  ?   �#     �#     �#     �#     �#     �#     $     $  *   $     G$     X$     e$     r$     x$     �$     �$  #   �$     �$     �$     �$     %     '%     9%     @%     S%     d%     v%     �%  !   �%     �%     �%     �%     �%  =   �%     &&     A&     _&     d&     }&     �&     �&     �&     �&     �&     �&      '     '     $'     ;'  
   A'     L'  B   Y'     �'  &   �'  %   �'  M   �'     @(     O(     \(     b(     q(  [   �(  &   �(     
)     )  4   ")  F   W)     �)  '   �)     �)     �)  	   �)  0   �)  0   "*     S*     a*     �*  	   �*     �*  X   �*  [   +  g   l+     �+     �+     �+     �+  h   ,     q,  A   y,     �,  >   �,  )   -  9   6-      p-  @   �-  )   �-  )   �-     &.     ).     ..  �  1.     0     /0     ;0     I0     Q0     W0     ]0     c0     i0     n0     w0     0     �0     �0  	   �0     �0  	   �0     �0     �0     �0     �0     �0     1     1     31     M1     _1     z1     �1     �1     �1     �1     �1     �1     �1  
   �1     2     2     2  �   %2  q   �2  H   3     a3     s3     �3     �3     �3     �3     �3     �3  
   �3  #   �3     "4     A4     Y4     p4  "   �4     �4     �4     �4     �4     5  0   /5  (   `5  "   �5     �5     �5     �5  "   �5     6  "    6  .   C6  E   r6  @   �6  �   �6  1   �7  1   �7  O   �7  S   O8     �8     #9     ,9  2   39  1   f9     �9     �9     �9     �9     �9  1   �9  0   +:     \:     h:     z:  B   �:  2   �:  8   ;     H;     N;  "   d;     �;  0   �;     �;     �;     �;     �;     �;     <      <     9<     U<     e<  5   s<  >   �<     �<  
   �<  
   �<     =  	   =     =     %=     .=     @=     ]=  +   s=     �=     �=     �=     �=     �=     �=     
>  /   >     J>     Q>  7   V>  @   �>     �>  T   �>     *?  ?   :?     z?     �?     �?     �?     �?     �?     �?  *   �?     �?     @     @     (@     .@     A@     N@  #   f@     �@     �@     �@     �@     �@     �@     �@     	A     A     ,A     =A  !   MA     oA     wA     �A     �A  =   �A     �A     �A     B     B     3B     HB     \B     jB     }B     �B     �B     �B     �B     �B     �B  
   �B     C  B   C     RC  &   [C  %   �C  M   �C     �C     D     D     D     'D  [   =D  &   �D     �D     �D  4   �D  F   E     TE  '   YE     �E     �E  	   �E  0   �E  0   �E     	F     F     6F  	   GF     QF  X   mF  [   �F  g   "G     �G     �G     �G     �G  h   �G     'H  A   /H     qH  >   �H  )   �H  9   �H      &I  @   GI  )   �I  )   �I     �I     �I     �I     O   g   �       K   �                           X   �   @   �   �   C       �           !   �   ~       "      -       �       �   &   /   �   �   L          �   x   l   v   I          �       '   �       �   U   6       �   �           e   �   +   �               d   �       1   ,      �      �   �         �   �   P       <   k   �   �      c       �   �   �   �             �   �   V       N           �   �   �   F       :   �   �       �   �       �   A   j   \   {   �       �   �   R   �          �   |       h   �   �   �   G   	       �   a      D              $   �       �   4           �   i   �   �       ?   �   =           �   2   �              �       �       �   ^   �       (   t   �   S   �   `       3   0   z   H   �                �   q   �   Z   �       M       *   �   r   �   5      o       E      �   �   7   �   ;   [   �   _   w           �   �   J   Q   �       �   ]   B       m           �   �   W      �      �       �   >   %   �       �      }   �   .               �      �           �   T   u   p   
               �       �   �   �       �           #   y      �      �   �      �   �       )   �   Y   s   �   �   �   n   �          b   �   �              �               �   �   8                  9       �   f   �       �       �   �                
(c) 2014-{} Juan Pablo Caram

 &Clear Plot &Command Line &Delete &Edit &Exit &File &Help &New &Options &Replot &Save Project &Tool &View &Zoom Fit &Zoom In &Zoom Out 2 (L/R) 2 (T/B) <b>Board cutout:</b> <b>Bounding Box:</b> <b>Create CNC Job:</b> <b>Create CNC Job</b> <b>Export G-Code:</b> <b>Isolation Routing:</b> <b>Mill Holes</b> <b>Non-copper regions:</b> <b>Offset:</b> <b>Paint Area:</b> <b>Plot Options:</b> <b>Scale:</b> <b>Tools</b> APPLICATION DEFAULTS About FlatCAM Add Arc Add Circle Add Path Add Polygon Add Rectangle After clicking here, click inside
the polygon you wish to be painted.
A new Geometry object with the tool
paths will be created. Algorithm to paint the polygon:<BR><B>Standard</B>: Fixed step inwards.<BR><B>Seed-based</B>: Outwards from seed. Amount by which to move the object
in the x and y axes in (x, y) format. Append to G-Code: Application to Object Application to Project Bottom Layer: Boundary Margin: Buffer selection 'b' CNC Job Object CNC Job Options Cancelled. Change the position of this object. Change the size of the object. Click on 1st corner ... Click on 1st point ... Click on 1st point... Click on 2nd point to complete ... Click on CENTER ... Click on a reference point ... Click on a reference point... Click on final location. Click on geometry to select Click on next point or hit SPACE to complete ... Click on opposite corner to complete ... Click on perimeter to complete ... Click on reference point. Click on target point. Combine Passes Combine all passes into one object Copy Objects 'c' Create Geometry for milling holes. Create a CNC Job object
for this drill object. Create a CNC Job object
tracing the contours of this
Geometry object. Create a Geometry object with
toolpaths to cut outside polygons. Create polygons covering the
areas without copper on the PCB.
Equivalent to the inverse of this
object. Can be used to remove all
copper from a specified region. Create the Geometry Object
for isolation routing. Create the Geometry Object
for milling toolpaths. Create toolpaths to cut around
the PCB and separate it from
the original board. Creates a Geometry objects with polygons
covering the copper-free areas of the PCB. Creates tool paths to cover the
whole area of a polygon (remove
all copper). You will be asked
to click on the desired polygon. Cut Path Cut Z: Cutting depth (negative)
below the copper surface. Cutting speed in the XY
plane in units per minute Delete Delete Shape '-' Depth of each pass (positive). Depth/pass: Diameter of the cutting tool. Diameter of the tool to
be used in the operation. Diameter of the tool to be
rendered in the plot. Direction:  Disable all plots Disable all plots but this one Distance by which to avoid
the edges of the polygon to
be painted. Distance from objects at which
to draw the cutout. Distance of the edges of the box
to the nearest polygon. Done. Double-Sided PCB Tool Draw polygons in different colors. Drawing Drill depth (negative)
below the copper surface. Duration [sec.]: Dwell: Edit Geometry Enable all plots Excellon Object Excellon Options Expected a DrawToolShape, got %s Expected a Geometry, got %s Export &SVG ... Export G-Code Export and save G-Code to
make this object to a file. Factor by which to multiply
geometric features of this object. Factor: Feed Rate: Feed rate: FlatCAM Object Gap size: Gaps: Generate Generate Geometry Generate the CNC Job object. Generate the CNC Job. Generate the geometry for
the board cutout. Genrate the Geometry object. Geometry Object Geometry Options Gerber Object Gerber Options Grid X distance Grid Y distante Height of the tool when
moving without cutting. Hello! Home How much (fraction of tool width)
to overlap each pass. How much (fraction) of the tool
width to overlap each tool pass. Idle. If the bounding box is 
to have rounded corners
their radius is equal to
the margin. Import &SVG ... Include tool-change sequence
in G-Code (Pause for tool change). Join Geometry Manual Margin: Max. magnet distance Measurement Tool Method: Mirror Axis: Mirror vertically (X) or horizontally (Y). Move Objects 'm' Multi-Depth: Multicolored Name: New Blank Geometry New Geometry Newer Version Available No active tool to respond to click! Not implemented. Nothing to move. Number of second to dwell. Object to Application Object to Project Offset Open &Excellon ... Open &Gerber ... Open &Project ... Open G-&Code ... Open recent ... Opens dialog to save G-Code
file. Options Overlap: PROJECT OPTIONS Pass overlap: Pause to allow the spindle to reach its
speed before cutting. Perform scaling operation. Perform the offset operation. Plot Plot (show) this object. Polygon Intersection Polygon Subtraction Polygon Union Prepend to G-Code: Project to Application Project to Object Rounded corners Save &Defaults Save Project &As ... Save Project C&opy ... Scale Seed-based Select 'Esc' Select from the tools section above
the tools you want to include. Selected Shape object has empty geometry (None) Shape objects has empty geometry ([]) Size of the gaps in the toolpath
that will remain to hold the
board in place. Snap to corner Snap to grid Solid Solid circles. Solid color polygons. Specify the edge of the PCB
by drawing a box around all
objects with this minimum
distance. Speed of the spindle
in RPM (optional) Spindle speed: Standard The diameter of the cutting
tool (just for display). There is a newer version of FlatCAM available for download:<br><br><B> Tool Tool Z where user can change drill bit
 Tool change Z: Tool change: Tool dia: Tool height when travelling
across the XY plane. Tool speed while drilling
(in units per minute). Toolchange Z: Tools in this Excellon object. Transfer options Travel Z: Type help to get started.

 Type here any G-Code commands you would
like to add at the beginning of the G-Code file. Type here any G-Code commands you would
like to add to the beginning of the generated file. Type here any G-Code commands you would
like to append to the generated file.
I.e.: M2 (End of program) Units: Update Geometry Update Plot Update the plot. Use multiple passes to limit
the cut depth in each pass. Will
cut multiple times until Cut Z is
reached. Vector: Where to place the gaps, Top/Bottom
Left/Rigt, or on all 4 sides. Width (# passes): Width of the isolation gap in
number (integer) of tool widths. Z-axis position (height) for
tool change. [error] Could not parse information about latest version. [success] FlatCAM is up to date! [warning] Failed checking for latest version. Could not connect. [warning] Invalid distance for buffering. [warning] Nothing selected for buffering. cw inch mm Project-Id-Version: FlatCam
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-09-04 10:28+0200
PO-Revision-Date: 2016-09-04 10:31+0200
Last-Translator: Daniel Sallin <info@daniel-stp.fr>
Language-Team: Daniel-STP <info@daniel-stp.fr>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.8
X-Poedit-SourceCharset: UTF-8
X-Poedit-Basepath: ../../..
X-Poedit-KeywordsList: translate_
X-Poedit-SearchPath-0: .
 
(c) 2014-{} Juan Pablo Caram

 &Clear Plot &Command Line &Delete &Edit &Exit &File &Help &New &Options &Replot &Save Project &Tool &View &Zoom Fit &Zoom In &Zoom Out 2 (L/R) 2 (T/B) <b>Board cutout:</b> <b>Bounding Box:</b> <b>Create CNC Job:</b> <b>Create CNC Job</b> <b>Export G-Code:</b> <b>Isolation Routing:</b> <b>Mill Holes</b> <b>Non-copper regions:</b> <b>Offset:</b> <b>Paint Area:</b> <b>Plot Options:</b> <b>Scale:</b> <b>Tools</b> APPLICATION DEFAULTS About FlatCAM Add Arc Add Circle Add Path Add Polygon Add Rectangle After clicking here, click inside
the polygon you wish to be painted.
A new Geometry object with the tool
paths will be created. Algorithm to paint the polygon:<BR><B>Standard</B>: Fixed step inwards.<BR><B>Seed-based</B>: Outwards from seed. Amount by which to move the object
in the x and y axes in (x, y) format. Append to G-Code: Application to Object Application to Project Bottom Layer: Boundary Margin: Buffer selection 'b' CNC Job Object CNC Job Options Cancelled. Change the position of this object. Change the size of the object. Click on 1st corner ... Click on 1st point ... Click on 1st point... Click on 2nd point to complete ... Click on CENTER ... Click on a reference point ... Click on a reference point... Click on final location. Click on geometry to select Click on next point or hit SPACE to complete ... Click on opposite corner to complete ... Click on perimeter to complete ... Click on reference point. Click on target point. Combine Passes Combine all passes into one object Copy Objects 'c' Create Geometry for milling holes. Create a CNC Job object
for this drill object. Create a CNC Job object
tracing the contours of this
Geometry object. Create a Geometry object with
toolpaths to cut outside polygons. Create polygons covering the
areas without copper on the PCB.
Equivalent to the inverse of this
object. Can be used to remove all
copper from a specified region. Create the Geometry Object
for isolation routing. Create the Geometry Object
for milling toolpaths. Create toolpaths to cut around
the PCB and separate it from
the original board. Creates a Geometry objects with polygons
covering the copper-free areas of the PCB. Creates tool paths to cover the
whole area of a polygon (remove
all copper). You will be asked
to click on the desired polygon. Cut Path Cut Z: Cutting depth (negative)
below the copper surface. Cutting speed in the XY
plane in units per minute Delete Delete Shape '-' Depth of each pass (positive). Depth/pass: Diameter of the cutting tool. Diameter of the tool to
be used in the operation. Diameter of the tool to be
rendered in the plot. Direction:  Disable all plots Disable all plots but this one Distance by which to avoid
the edges of the polygon to
be painted. Distance from objects at which
to draw the cutout. Distance of the edges of the box
to the nearest polygon. Done. Double-Sided PCB Tool Draw polygons in different colors. Drawing Drill depth (negative)
below the copper surface. Duration [sec.]: Dwell: Edit Geometry Enable all plots Excellon Object Excellon Options Expected a DrawToolShape, got %s Expected a Geometry, got %s Export &SVG ... Export G-Code Export and save G-Code to
make this object to a file. Factor by which to multiply
geometric features of this object. Factor: Feed Rate: Feed rate: FlatCAM Object Gap size: Gaps: Generate Generate Geometry Generate the CNC Job object. Generate the CNC Job. Generate the geometry for
the board cutout. Genrate the Geometry object. Geometry Object Geometry Options Gerber Object Gerber Options Grid X distance Grid Y distante Height of the tool when
moving without cutting. Hello! Home How much (fraction of tool width)
to overlap each pass. How much (fraction) of the tool
width to overlap each tool pass. Idle. If the bounding box is 
to have rounded corners
their radius is equal to
the margin. Import &SVG ... Include tool-change sequence
in G-Code (Pause for tool change). Join Geometry Manual Margin: Max. magnet distance Measurement Tool Method: Mirror Axis: Mirror vertically (X) or horizontally (Y). Move Objects 'm' Multi-Depth: Multicolored Name: New Blank Geometry New Geometry Newer Version Available No active tool to respond to click! Not implemented. Nothing to move. Number of second to dwell. Object to Application Object to Project Offset Open &Excellon ... Open &Gerber ... Open &Project ... Open G-&Code ... Open recent ... Opens dialog to save G-Code
file. Options Overlap: PROJECT OPTIONS Pass overlap: Pause to allow the spindle to reach its
speed before cutting. Perform scaling operation. Perform the offset operation. Plot Plot (show) this object. Polygon Intersection Polygon Subtraction Polygon Union Prepend to G-Code: Project to Application Project to Object Rounded corners Save &Defaults Save Project &As ... Save Project C&opy ... Scale Seed-based Select 'Esc' Select from the tools section above
the tools you want to include. Selected Shape object has empty geometry (None) Shape objects has empty geometry ([]) Size of the gaps in the toolpath
that will remain to hold the
board in place. Snap to corner Snap to grid Solid Solid circles. Solid color polygons. Specify the edge of the PCB
by drawing a box around all
objects with this minimum
distance. Speed of the spindle
in RPM (optional) Spindle speed: Standard The diameter of the cutting
tool (just for display). There is a newer version of FlatCAM available for download:<br><br><B> Tool Tool Z where user can change drill bit
 Tool change Z: Tool change: Tool dia: Tool height when travelling
across the XY plane. Tool speed while drilling
(in units per minute). Toolchange Z: Tools in this Excellon object. Transfer options Travel Z: Type help to get started.

 Type here any G-Code commands you would
like to add at the beginning of the G-Code file. Type here any G-Code commands you would
like to add to the beginning of the generated file. Type here any G-Code commands you would
like to append to the generated file.
I.e.: M2 (End of program) Units: Update Geometry Update Plot Update the plot. Use multiple passes to limit
the cut depth in each pass. Will
cut multiple times until Cut Z is
reached. Vector: Where to place the gaps, Top/Bottom
Left/Rigt, or on all 4 sides. Width (# passes): Width of the isolation gap in
number (integer) of tool widths. Z-axis position (height) for
tool change. [error] Could not parse information about latest version. [success] FlatCAM is up to date! [warning] Failed checking for latest version. Could not connect. [warning] Invalid distance for buffering. [warning] Nothing selected for buffering. cw inch mm 