��    T     �
  �  \      �  @   �     �     �     �                          &     ,     2     7     @     H     V     \  	   b     l  	   u          �     �     �     �     �     �               6     H     c     r     �     �     �     �     �     �  
   �     �     �        �     q   �        G         Z   H   ^      �      �      �   2   �      !     .!     =!     K!     \!     `!     g!     x!     �!     �!     �!     �!  
   �!  #   �!     �!  v   "     �"     �"     �"     �"  "   �"     #     -#     L#     j#     �#  0   �#  (   �#  "   �#     $     6$     M$     S$  "   b$     �$     �$     �$     �$     �$     �$     �$  "   %  .   +%  E   Z%  @   �%  �   �%  1   �&  1   �&  O   �&  S   7'  \   �'     �'     h(     q(  <   x(  2   �(  1   �(     )     *)     1)     B)     a)     m)     v)  .   �)  1   �)  0   �)     &*     2*     D*  B   c*  2   �*  8   �*     +     +  =   .+  "   l+     �+  0   �+     �+     �+     �+  $   �+  %   ,     8,     F,     W,     _,     o,      �,     �,     �,     �,     �,  5   �,  >   '-     f-     n-  -   �-     �-  
   �-  
   �-     �-     .     .     +.     G.  &   P.  	   w.     �.     �.     �.     �.     �.  +   �.     /     /     2/     B/     S/     a/     p/     �/  /   �/     �/     �/  7   �/  @   0  $   E0     j0  T   p0     �0  ?   �0     1     41     B1     K1     a1     h1     p1     �1     �1     �1     �1  *   �1  n   �1     S2     d2     q2     ~2     �2     �2     �2  #   �2     �2     �2     3     3     53     H3     ^3     p3     w3     �3     �3     �3     �3     �3     �3  !   �3     	4     4     4     *4     04     >4     N4     `4  =   n4     �4     �4  )   �4  <   5     L5     Q5     j5  
   p5     {5     �5     �5     �5     �5     �5     �5     �5     6     (6     =6     T6     f6     v6     ~6     �6     �6     �6  
   �6     �6  
   �6     �6  !   �6  #   7  B   <7     7  
   �7  &   �7  %   �7     �7  M   �7     58     D8     Q8     W8     f8  [   |8  �   �8  &   g9     �9     �9     �9  o   �9  4   %:  F   Z:     �:  '   �:     �:     �:  	   �:  0   �:  0   %;     V;     d;     �;  	   �;     �;  X   �;  [   <  g   o<     �<     �<     �<     =     =     =  h   0=     �=     �=  A   �=      >  >   >  )   Q>  2   {>     �>  %   �>  9   �>  (   )?     R?  *   j?     �?      �?  '   �?  &   �?     #@  (   B@  )   k@  )   �@  #   �@      �@  @   A  )   EA  ;   oA     �A  )   �A  (   �A  $   B     ?B     BB     GB  h  JB  T   �D     E     (E     <E     KE  	   ZE     dE     pE     vE     }E     �E  	   �E     �E     �E  
   �E     �E     �E     �E     �E     �E     �E  0   �E     0F     IF     ^F     wF     �F     �F     �F     �F     �F     G     G     6G     IG     ZG     qG     G     �G     �G     �G     �G  �   �G  �   ~H     �H  S   I     hI  F   mI     �I     �I     �I  5   �I     /J     EJ     TJ     aJ     sJ     wJ     ~J     �J     �J     �J     �J     �J     �J  "   �J  !   K  �   ?K  #   �K     �K      L     ,L  7   IL     �L  #   �L  #   �L      �L  %   M  W   (M  A   �M  3   �M     �M     N  
   0N     ;N  *   RN  
   }N     �N     �N     �N     �N  -   �N  !   	O  -   +O  4   YO  F   �O  S   �O  �   )P  2   �P  2   Q  F   LQ  _   �Q  v   �Q  �   jR     S     S  =   #S  7   aS  ;   �S  "   �S     �S     T  "   T     6T     GT  !   ST  6   uT  D   �T  =   �T  
   /U     :U  $   QU  G   vU  @   �U  A   �U  	   AV     KV  V   gV  .   �V     �V  4   �V     +W     <W     JW  7   WW  L   �W     �W     �W     	X     X     "X  '   4X  0   \X     �X     �X     �X  A   �X  P   Y     ^Y  *   fY  ;   �Y  .   �Y     �Y     Z  (   .Z     WZ     fZ     zZ     �Z  1   �Z     �Z  
   �Z     �Z     �Z     [     *[  9   @[     z[     �[     �[     �[     �[     �[     �[     \  >   \     V\     ]\  C   b\  `   �\  4   ]  	   <]  G   F]     �]  Y   �]      �]     ^     /^      7^     X^     a^     g^     �^     �^     �^     �^  *   �^  ~   �^     a_     u_  
   �_     �_     �_     �_  $   �_  4   �_     `     &`     9`     Y`     r`     �`     �`     �`     �`     �`     �`     �`     a     a     7a  4   Ca     xa     �a     �a     �a     �a     �a     �a     �a  J   �a     Fb  !   cb  H   �b  W   �b     &c     +c     Hc     Nc     Zc     oc     �c     �c     �c     �c  $   �c     d     d     4d     Ld     cd     ud     �d     �d     �d     �d  "   �d     e  
   e     e     *e  /   8e  2   he  X   �e     �e      f  )   	f  &   3f     Zf  k   bf     �f     �f     �f     g     g  k   /g  �   �g  /   =h     mh     �h     �h  �   �h  =   i  K   ]i  	   �i  4   �i     �i     �i     j  D   "j  C   gj     �j  $   �j     �j     �j  2   k  \   8k  b   �k  y   �k  
   rl     }l     �l     �l     �l     �l  �   �l     }m  !   �m  _   �m     n  R   n  6   qn  a   �n  #   
o  6   .o  L   eo  L   �o     �o  7   p  %   Sp  (   yp  /   �p  7   �p  $   
q  .   /q  L   ^q  :   �q  0   �q     r  ^   4r  4   �r  H   �r      s  3   2s  *   fs  $   �s     �s     �s     �s         �             2   �   @  �   �       N         �   �          *  "      �   �   _   �          �   �   �       L   �          x     �       5  y   �   �   �   �   K     �           a   �       &   :   (      t   �                  3       �   H  �   �   r     I   n           p      !  �         Z   )       �         �   �          �   �           $  k   M          %  G  �           q   �   g   �       �   o   z   �   E   �     #  �      W   
      �       �   T      Q      �      !   �   �   5   �   u   -         K      >   A  .  �   F  �       �       �   :  �       s   <   /       I    1   m   �   ;  �   M   G   �   E        8   c   '           �      7   V       D         �   �   '  �   �   8      @   �   0          R  Y   `   T           C  �   �                  �   �   R   \   S   �   {   P  �       �   ,  +                      �       4   =                �   �   �   �   U      w   �   �   H     v       �        �             *     <  �   �   i   �   )          1  �   �       �   
           �   �   �                  B       ,   "   [      3  �           -   �   �     �                  �   O  }       l   �   S      D  2  �       �   f   �   �     �           X   �   �                 �   �                      �   �      �   .   6  (   P   d              b          �   �           B  A   �       7  O   �   Q      9           �   $   e   �   ^   4  +   �               >  #   ?  L  J   �   9          �      =   �   �   �   �       C       �   ?   0  ]   �       �          /    	          J     h   �           �       �   F   �                        �   �      N  �       |   6   &    	   �       �   ;             �   %   ~   j   �       �    

Type help <command_name> for usage.
 Example: help open_gerber 
(c) 2014-{} Juan Pablo Caram

 %d processes running. &Clear Plot &Command Line &Delete &Edit &Exit &File &Help &New &Options &Replot &Save Project &Tool &View &Zoom Fit &Zoom In &Zoom Out 2 (L/R) 2 (T/B) <B>Change project units ...</B> <b>Board cutout:</b> <b>Bounding Box:</b> <b>Create CNC Job:</b> <b>Create CNC Job</b> <b>Export G-Code:</b> <b>Isolation Routing:</b> <b>Mill Holes</b> <b>Non-copper regions:</b> <b>Offset:</b> <b>Paint Area:</b> <b>Plot Options:</b> <b>Scale:</b> <b>Tools</b> APPLICATION DEFAULTS About FlatCAM Add Arc Add Circle Add Path Add Polygon Add Rectangle After clicking here, click inside
the polygon you wish to be painted.
A new Geometry object with the tool
paths will be created. Algorithm to paint the polygon:<BR><B>Standard</B>: Fixed step inwards.<BR><B>Seed-based</B>: Outwards from seed. Alignment Holes: Alignment holes (x1, y1), (x2, y2), ... on one side of the mirror axis. All Amount by which to move the object
in the x and y axes in (x, y) format. Append to G-Code: Application to Object Application to Project Attempting to create a FlatCAM CNCJob Object from  Available commands:
 Axis Location: Bottom Layer: Boundary Margin: Box Buffer Buffer Selection Buffer distance: Buffer selection 'b' CNC Job Object CNC Job Options CNCjob created: %s Cancelled. Change the position of this object. Change the size of the object. Changing the units of the project causes all geometrical properties of all objects to be scaled accordingly. Continue? Choose an item from Project Click on 1st corner ... Click on 1st point ... Click on 1st point... Click on 2nd point to complete ... Click on CENTER ... Click on a reference point ... Click on a reference point... Click on final location. Click on geometry to select Click on next point or hit SPACE to complete ... Click on opposite corner to complete ... Click on perimeter to complete ... Click on reference point. Click on target point. Close Combine Passes Combine all passes into one object Connect: Contour: Converted units to %s Converting units to  Copy Objects 'c' Could not load defaults file. Create Alignment Drill Create Geometry for milling holes. Create a CNC Job object
for this drill object. Create a CNC Job object
tracing the contours of this
Geometry object. Create a Geometry object with
toolpaths to cut outside polygons. Create polygons covering the
areas without copper on the PCB.
Equivalent to the inverse of this
object. Can be used to remove all
copper from a specified region. Create the Geometry Object
for isolation routing. Create the Geometry Object
for milling toolpaths. Create toolpaths to cut around
the PCB and separate it from
the original board. Creates a Geometry objects with polygons
covering the copper-free areas of the PCB. Creates an Excellon Object containing the specified alignment holes and their mirror images. Creates tool paths to cover the
whole area of a polygon (remove
all copper). You will be asked
to click on the desired polygon. Cut Path Cut Z: Cut around the perimeter of the polygon
to trim rough edges. Cutting depth (negative)
below the copper surface. Cutting speed in the XY
plane in units per minute Defaults saved. Delete Delete Shape '-' Depth of each pass (positive). Depth/pass: Diameter Diameter of the cutting tool. Diameter of the drill for the alignment holes. Diameter of the tool to
be used in the operation. Diameter of the tool to be
rendered in the plot. Direction:  Disable all plots Disable all plots but this one Distance by which to avoid
the edges of the polygon to
be painted. Distance from objects at which
to draw the cutout. Distance of the edges of the box
to the nearest polygon. Done. Double-Sided PCB Tool Draw lines between resulting
segments to minimize tool lifts. Draw polygons in different colors. Drawing Drill depth (negative)
below the copper surface. Drill diam.: Duration [sec.]: Dwell: ERROR: Could not load defaults file. ERROR: Failed to parse defaults file. Edit Geometry Enable all plots English Excellon Object Excellon Options Expected a DrawToolShape, got %s Expected a Geometry, got %s Export &SVG ... Export G-Code Export SVG cancelled. Export and save G-Code to
make this object to a file. Factor by which to multiply
geometric features of this object. Factor: Failed to create CNCJob Object Failed to open recent files file for writing. Failed to parse defaults file. Feed Rate: Feed rate: File exists. Overwrite? FlatCAM Object FlatCAM Starting... Follow geometry created: %s Francais G-Code file failed during processing:
 Gap size: Gaps: Generate Generate Geometry Generate the CNC Job object. Generate the CNC Job. Generate the geometry for
the board cutout. Generating CNC Job. Genrate the Geometry object. Geometry Object Geometry Options Gerber Object Gerber Options Grid X distance Grid Y distante Height of the tool when
moving without cutting. Hello! Home How much (fraction of tool width)
to overlap each pass. How much (fraction) of the tool
width to overlap each tool pass. How to select the polygons to paint. Idle. If the bounding box is 
to have rounded corners
their radius is equal to
the margin. Import &SVG ... Include tool-change sequence
in G-Code (Pause for tool change). Isolation geometry created: %s Join Geometry Language Layer to be mirrorer. Manual Margin: Max. magnet distance Measurement Tool Method: Mirror Axis: Mirror Object Mirror vertically (X) or horizontally (Y). Mirrors (flips) the specified object around the specified axis. Does not create a new object, but modifies it. Move Objects 'm' Multi-Depth: Multicolored Name: New Blank Geometry New Geometry Newer Version Available No active tool to respond to click! Not implemented. Nothing to move. Number of second to dwell. Object (%s) created: %s Object deleted: %s Object to Application Object to Project Offset Open &Excellon ... Open &Gerber ... Open &Project ... Open G-&Code ... Open cancelled. Open recent ... Opened:  Opens dialog to save G-Code
file. Options Overlap: PROJECT OPTIONS Paint Paint Options Paint selection Painting polygon. Pass overlap: Pause to allow the spindle to reach its
speed before cutting. Perform scaling operation. Perform the offset operation. Please Select a Geometry object to export Please select one or more tools from the list and try again. Plot Plot (show) this object. Point Point/Box: Polygon Intersection Polygon Paint started ... Polygon Subtraction Polygon Union Prepend to G-Code: Project Project copy saved to:  Project loaded from:  Project saved to:  Project saved to: %s Project to Application Project to Object Rounded corners Russian Save &Defaults Save Project &As ... Save Project As ... Save Project C&opy ... Saved to:  Scale Seed-based Select 'Esc' Select a Geometry Object to edit. Select a Geometry Object to update. Select from the tools section above
the tools you want to include. Selected Selection: Shape object has empty geometry (None) Shape objects has empty geometry ([]) Single Size of the gaps in the toolpath
that will remain to hold the
board in place. Snap to corner Snap to grid Solid Solid circles. Solid color polygons. Specify the edge of the PCB
by drawing a box around all
objects with this minimum
distance. Specify the point (x, y) through which the mirror axis passes or the Geometry object containing a rectangle that the mirror axis cuts in half. Speed of the spindle
in RPM (optional) Spindle speed: Standard Straight lines The axis should pass through a <b>point</b> or cut a specified <b>box</b> (in a Geometry object) in the middle. The diameter of the cutting
tool (just for display). There is a newer version of FlatCAM available for download:<br><br><B> Tool Tool Z where user can change drill bit
 Tool change Z: Tool change: Tool dia: Tool height when travelling
across the XY plane. Tool speed while drilling
(in units per minute). Toolchange Z: Tools in this Excellon object. Transfer options Travel Z: Type help to get started.

 Type here any G-Code commands you would
like to add at the beginning of the G-Code file. Type here any G-Code commands you would
like to add to the beginning of the generated file. Type here any G-Code commands you would
like to append to the generated file.
I.e.: M2 (End of program) Units: Unknown command
 Unknown command: %s Update Geometry Update Plot Update the plot. Use multiple passes to limit
the cut depth in each pass. Will
cut multiple times until Cut Z is
reached. Vector: WARNING: No object selected. Where to place the gaps, Top/Bottom
Left/Rigt, or on all 4 sides. Width (# passes): Width of the isolation gap in
number (integer) of tool widths. Z-axis position (height) for
tool change. [error] An internal error has ocurred. See shell.
 [error] Cannot open file:  [error] Could not load defaults file. [error] Could not parse information about latest version. [error] Failed to load recent item list. [error] Failed to open  [error] Failed to open file for saving: %s [error] Failed to open file:  [error] Failed to open file: %s  [error] Failed to open project file: %s [error] Failed to parse defaults file. [error] Failed to parse file:  [error] Failed to parse project file: %s [error] Failed to parse recent item list. [error] Failed to write defaults to file. [error] No geometry found in file:  [success] FlatCAM is up to date! [warning] Failed checking for latest version. Could not connect. [warning] Invalid distance for buffering. [warning] Milling tool is larger than hole size. Cancelled. [warning] No polygon found. [warning] Nothing selected for buffering. [warning] Nothing selected for painting. [warning] There was no active object cw inch mm Project-Id-Version: FlatCam
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2017-01-29 08:59+0100
PO-Revision-Date: 2017-01-29 09:05+0100
Last-Translator: Carsten Schoenert <c.schoenert@t-online.de>
Language-Team: 
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.11
X-Poedit-SourceCharset: UTF-8
X-Poedit-Basepath: ../../..
X-Poedit-KeywordsList: translate_
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: bugs
X-Poedit-SearchPathExcluded-1: doc
X-Poedit-SearchPathExcluded-2: FlatCAM_GTK
 

Schreiben Sie 'help <command>' für die Benutzung.
 Zum Beispiel: help open_gerber 
(c) 2014-{} Juan Pablo Caram

 %d Prozesse laufen. &Plot Löschen &Kommandozeile &Löschen &Bearbeiten &Exit &Datei &Hilfe &Neu &Optionen &Neu Plotten Projekt &Speichern &Werkzeuge &Anzeige Passend &Zoomen &Zoom In Heraus &Zoomen 2 (L/R) 2 (O/U) <B>Ändern der Maßeinheiten vom Projekt ...</B> <b>Board Ausschnitt:</b> <b>Bounding Box:</b> <b>Erstelle CNC Job:</b> <b>Erstelle CNC Job</b> <b>Exportire G-Code:</b> <b>Isolationsrouting:</b> <b>Löcher fräsen</b> <b>Nicht Kupfer Regionen:</b> <b>Offset:</b> <b>Zeichnungsfeld:</b> <b>Optionen Plotten:</b> <b>Skalierung:</b> <b>Werkzeuge</b> Standard Einstellungen Über FlatCAM Kreisbogen hinzufügen Kreis hinzufügen Pfad hinzufügen Polygon hinzufügen Rechteck hinzufügen Nachdem Sie hier geklickt haben wählen Sie
das Polygon an welches ausgefüllt werden soll.
Ein neues Geometrieobjekt mit den
Werkzeugpfaden wird erstellt. Algorithmus zum Zeichen der Polygone:<BR><B>Standard</B>: Fixe Schrittweite innen.<BR><B>Samen basiert</B>: Auswärts vom Samen. Asurichtungslöcher: Ausrichtungslöcher (x1, y1), (x2, y2), ... auf einer Seite der gespiegelten Achse. Alle Versatz der X und Y Achsen um welche
das Objekt bewegt werden muss im. Zum G-Code hinzufügen: Applikation zu Objekt Applikation zu Projekt Versuche ein FlatCAM CNCJob Objekt zu erstellen von   Mögliche Kommandos:
 Lage der Achse Untere Lage: Boundary Abstand: Box Buffer Buffer Auswahl Buffer Abstand Auswahl Zwischenspeicher 'b' CNC Job Objekt CNC Job Optionen CNCjob erstellt: %s Abgebrochen. Ändern der Position des Objektes. Ändern der Größe des Objektes. Ein Wechsel der Maßeinheiten vom Projekt wird ein Skalieren aller geometrischen Eigenschaften der Objekte nach sich ziehen. Fortsetzen? Wählen Sie ein Element vom Projekt Auf die erste Ecke klicken ... Auf den ersten Punkt klicken ... Auf den 1. Punkt klicken ... Auf den zweiten Punkt zum Vervollständigen klicken ... Ins ZENTRUM klicken ... Auf einen Referenzpunkt klicken ... Auf einen Referenzpunkt klicken ... Auf die finale Position klicken. Zur Auswahl auf die Geometrie klicken Auf den nächsten Punkt klicken oder die Leertaste betätigen zum Vervollständigen ... Auf die gegenüberliegende Ecke klicken zum Vervollständigen ... Auf den Perimeter klicken zum Vervollständigen ... Auf den Referenzpunkt klicken. Auf den Zielpunkt klicken. Schließen Kombiniere Durchläufe Kombiniert alle Durchgänge in ein Objekt. Verbinden: Kontur: Einheiten konvertiert zu %s Einheiten konvertieren zu Objekte kopieren 'c' Konnte die Standardeinstellungen nicht laden. Erstelle Bohrungen zum Ausrichten Erstelle Geometrie für zu fräsende Löcher. Erstellt ein CNC Job Objekt
für dieses Bohr Objekt. Erstell einen CNC Job
der den Konturen dieses
Geometrieobjektes folgt. Erstellt ein Geometrieobjekt mit
Werkzeugwegen um Polygone außerhalb
zu schneiden. Erstellt Polygone welche die Flächen ohne
Kupfer auf dem Board abdecken. Diese sind
invers zum aktuellen Objekt. Kann benutzt werden
um Kupfer von einer spezifischen Fläche zu entfernen. Erstellt die Geometrie für
das Isolationsrouting. Erstellt das Geometrieobjekt
für die Fräsbahnen. Erstellt einen Wegepfad um die PCB
vom Original Board aus zuschneiden. Erstellt eine Geometrie der Objekte mit
Polygonen der Kupfer freien Zonen auf der Leiterplatte. Erstellt ein Excellon Objekt welches die spezifizierten ausgerichteten Löcher und deren gespiegelten Images enthält. Erstellt einen Wegeplan um die komplette
Fläche des Polygons abzudecken
(entfernen des Kupfers). Sie werden gefragt
um das gewünschte Polygon auszuwählen. Pfad ausschneiden Tiefe Z: Den Rand des Polygon ausschneiden
um harte Kanten zu trimmen. Schnitttiefe (negativ)
unterhalb der Kupferoberfläche. Schrittgeschwindigkeit der XY
Ebene in Einheiten pro Minute Standardeinstellungen gespeichert. Löschen Form löschen '-' Tiefe jedes Durchgangs (positive). Tiefe/Durchgang: Durchmesser Durchmesser des Schnittwerkzeugs. Durchmesser der Bohrungen für die Ausrichtungslöcher Durchmesser des Werkzeuges welches
in dieser Operation benutzt wird. Durchmesser des Werkzeuges das in
diesem Plot gerendert wird. Richtung:  Deaktiviere alle Plots Deaktiviere alle Plots außer diesen Abstand an den Ecken des Polygons
an dem nichts gezeichnet werden soll. Distanz vom Objekt an dem der
Ausschnitt gezeichnet werden soll. Abstand der Ecken von der Box
zum am nähesten gelegenen Polygon. Erledigt. Doppelseitiges PCB Werkzeug Zeichnet Linien zwischen resultierenden
Segmenten um Werkzeugbewegungen zu
minimieren. Zeichnet Polygone in unterschiedlichen Farben. Zeichnen Bohrtiefe (negativ)
unterhalb der Kupferoberfläche. Bohrdurchmesser: Dauer [sec.]: Vorlaufzeit: Fehler: Konnte die Datei mit den Standards nicht laden. Fehler: Die Datei mit den Standardeinstellungen konnte nicht geparst werden. Geometrie bearbeiten Alle Plots ermöglichen Englisch Excellon Objekt Optionen Excellon Ein DrawToolShape erwartet, erhalten %s Eine Geometrie erwartet, stattdessen %s erhalten Exportiere SV&G ... Exportiere G-Code Export SVG abgebrochen. Exportiert und speichert den G-Code
aus dem Objekt in eine Datei. Faktor mit dem die geometrischen
Features in diesem Objekt multipliziert
werden. Faktor: CNCJob Objekt konnte nicht erstellt werden Aktuelle Datei konnte nicht zum Schreiben geöffnet werden. Konnte die Standardeinstellungen nicht parsen. Vorschubgeschwindigkeit: Vorschubgeschwindigkeit: Datei existiert bereits, überschreiben? FlatCAM Objekt FlatCAM Startet ... Folgegeometrie erstellt: %s Französisch G-Code Datei Fehler während der Prozession von:
 Größe Abstand: Abstände: Erstelle Erstelle Geometrie Erstelle das CNC Job Objekt. Erstelle den CNC Job. Erstellt die Geometrie für
den Leiterplatten Ausschnitt. Erstelle den CNC Job. Erstelle das Geometrieobjekt. Geometrieobjekt Optionen Geometrie Gerber Objekt Optionen Gerber Raster X Entfernung Raster Y Entfernung Höhe des Werkzeugs wenn dieses
bewegt wird ohne zu schneiden. Hallo! Home Wieviel Überlappung pro Durchlauf 
(Bruchteil der Werkzeugbreite). Welcher Anteil von der Werkzeugbreite
soll bei der Überlappung eines Durchgangs
benutzt werden. Wie sollen Polygons zum Zeichnen ausgewählt werden. Leerlauf. Wenn die Box gerundete
Ecken besitzt ist der Radius
gleich dem Abstand, Importiere S&VG ... Einfügen der Wechselsequenz in den
G-Code für das Werkzeug (Pause zum
Werkzeugwechsel). Isolationsgeometrie erstellt: %s Geometrie starten Sprache Lage die gespiegelt werden soll. Handbuch Rand: Max. magnetische Entfernung Messwerkzeug Methode: Spiegelachse: Objekt spiegeln Vertikal (X) oder Horizontal (Y) spiegeln. Spiegelt das ausgewählte Objekt um die spezifizierte Achse. Dies erstellt kein neues Objekt, modifiziert das vorhandene aber. Objekte bewegen 'm' Multi-Tiefe: Merhfarbig Name: Neue leere Geometrie Neue Geometrie Eine neuere Version ist erhältlich. Kein aktives Werkzeug zum Antworten auf einen Klick! Nicht implementiert. Nichts zu bewegen. Anzahl Verweilzeit in Sekunden. Objekt (%s) erstellt: %s Objekt gelöscht: %s Objekt zu Applikation Objekt zu Projekt Offset Öffne &Excellon ... Öffne &Gerber ... Öffne &Projekt ... Öffne G-&Code ... Öffnen abgebrochen. Öffne zuletzt verwendet ... Geöffnet:  Öffnet den Dialog zum Abspeichern
der G-Code Datei. Optionen Überlappung: Projekt Optionen Zeichnen Zeichnungsoptionen Zeichnungsauswahl Polygon zeichnen. Überlappung je Durchlauf: Pause damit die Spindel ihre Geschwindigkeit
erreicht bevor gefräst wird. Durchführen der Skalierung. Durchführen des Offsetabgleichs. Bitte wählen Sie das Geometrieobjekt aus welches exportiert werden soll Bitte wählen Sie ein oder mehrere Werkzeuge von der Liste und versuchen Sie es erneut. Plot Plotte (zeige) diese Objekt. Punkt Punkt/Zone: Schnittmenge Polygon Polygon zeichnen gestartet ... Differenzierung Polygon Zusammenführung Polygon Voranstellen zum G-Code: Projet Projektkopie wurde gespeichert nach: Projekt geladen von:  Projekt gespeichert nach: Projekt gespeichert: %s Projekt zu Applikation Projekt zu Objekt Gerundete Ecken Russisch Speichere Standard&werte Speichere Projekt &unter ... Speichere Projekt unter ... Speichere K&opie des Projektes ... Gespeichert:  Skalierung Samen basiert Auswahl 'Esc' Wählen Sie ein Geometrieobjekt zum Bearbeiten. Wählen Sie ein Geometrieobjekt zum Aktualisieren. Wählen Sie oben die Werkzeuge in der Sektion
der Werkzeuge aus die Sie benutzen wollen. Ausgewählt Auswahl: Die Form hat eine leere Geometrie (Keine) Die Form hat eine leere Geometrie ([]) Einfach Größe des Abstandes zwischen den
Werkzeugwegen die erhalten bleibt
um das Board auf der Stelle zu halten. An der Ecke einrasten Am Raster ausrichten Einheitlich Ausgefüllte Kreise. Einfarbige Polygone. Spezifiziert die Kante der Leiterplatte
durch Zeichnen einer Box um alle Objekte
mit dem kleinsten Abstand. Legt den Punkt (x, y) fest durch den die Spiegelachse läuft oder das Geometrieobjekt mit dem Rechteck das durch die Achse in zwei gleiche Hälften geteilt wird. Geschwindigkeit der Spindel
in U/min (optional) Geschwindigkeit Spindel: Standard Gerade Linien Die Achse sollte durch einen <b>Punkt</b> laufen oder eine spezifische <b>Box</b> (in einem Geometrieobjekt) in der Mitte teilen. Der Durchmesser des
Schnittwerkzeuges (nur für die Anzeige). Es ist eine neuere Version von FlatCAM erhältlich per Download:<br><br><B> Werkzeuge Werkzeug Z wo der Benutzer den Bohrer wechseln kann
 Werkzeugwechsel Z: Werkzeugwechsel: Durchmesser Werkzeug: Anhebung des Werkzeugs wenn auf
dieses auf der XY Ebene bewegt wird. Geschwindigkeit des Werkzeugs
während des Bohrens (Umdrehungen/s). Werkzeugwechsel Z: Werkzeuge in diesem Excellon Objekt. Optionen Übertragung Anhebung Z: Schreiben Sie zum Beispiel 'help' um zu starten.

 Platzieren Sie hier die G-Code Komandos die
Sie am Beginn der G-Code Datei schreiben wollen. Geben Sie hier die G-Code Kommandos ein den
Sie am Beginn der erstellten Datei hinzufügen wollen. Platzieren Sie hier die G-Code Komandos die
Sie ans Ende der G-Code Datei anhängen wollen.
z.B.: M2 (Ende des Programms) Einheiten: Unbekanntes Kommando
 Unbekanntes Kommando: %s Geometrie aktualisieren Aktualsiere Plot Plot aktualisieren. Benutzt mehrere Durchgänge um die
Schnitttiefe durch einzelne Durchgänge zu
erreichen. Es wird mehrmals geschnitten
bis die Tiefe Z erreicht ist. Vektor: Warnung: Kein Objekt ausgewählt. Ort an dem Abstände eingefügt werden sollen,
Oben/Unten, Links/Rechts oder allen vier Seiten. Weite (# Durchläufe): Ganzzahlige Anzahl der Breiten vom Werkzeug
zur Erstellung des Isolationsabstandes Position der Z-Achse (Höhe) für
den Werkzeugwechsel. [Fehler] Ein interner Fehler ist aufgetreten. Bitte Sehen Sie in die Ausgabe des Shell Fensters.
 [Fehler] Kann Datei nicht öffnen:  [Fehler] Konnte die Standardeinstellungen nicht laden. [Fehler] Konnte die Informationen über die aktuelle Version nicht einlesen. [Fehler] Die Liste der zuletzt benutzen Dateien konnte nicht geladen werden. [Fehler] Kann nicht öffnen [Fehler] Konnte Datei nicht öffnen zum abspeichern: %s [Fehler] Konnte Datei nicht öffnen:  [Fehler] Konnte Datei nicht öffnen: %s  [Fehler] Konnte Projekt Datei nicht öffnen: %s [Fehler] Konnte die Standardeinstellungen nicht parsen. [Fehler] Konnte Datei nicht parsen:  [Fehler] Konnte Projekt Datei nicht parsen: %s [Fehler] Die Liste der zuletzt benutzen Dateien konnte nicht geparst werden. [Fehler] Konnte die Standardeinstellungen nicht speichern. [Fehler] Keine Geometrie in der Datei gefunden:  [Erfolg] FlatCAM is aktuell! [Warnung] Ein Fehler ist aufgetreten beim Prüfen der aktuellen Version. Kann nicht verbinden. [Warnung] Ungültiger Abstand zum Zwischenspeichern. [Warnung] Fräswerkzeug ist größer als die Größe des Lochs. Abbruch. [Warnung] Kein Polygon gefunden. [Warnung] Nichts ausgewählt zum Zwischenspeichern. [Warnung] Nichts ausgewählt zum Zeichnen. [Warnung] Es gab kein aktives Objekt cw Inch mm 