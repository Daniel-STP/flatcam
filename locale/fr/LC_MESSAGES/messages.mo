��    �      �  K  ,      X     Y     y     �     �     �     �     �     �     �     �     �     �     �     �  	   �     �  	   �                     %     :     Q     g     }     �     �     �     �     �     �     	          +     9  
   A     L     U     a  �   o  q   �  H   b     �     �     �     �     �     	          -  
   =  #   H     l     �     �     �  "   �     �          &     D     ]  0   y  (   �  "   �     �          '  "   6     Y  "   j  .   �  E   �  @     �   C  1   �  1     O   I  S   �     �     m     v  2   }  1   �     �     �     �          %  1   C  0   u     �     �     �  B   �  2   &  8   Y     �     �  "   �     �  0   �     
            "      0      A      Q       b      �      �      �   5   �   >   �      2!  
   :!  
   E!     P!  	   _!     i!     o!     x!     �!     �!  +   �!     �!     "     "     '"     5"     D"     T"  /   d"     �"     �"  7   �"  @   �"     #  T   #     t#  ?   �#     �#     �#     �#     �#     �#     $     $  *   $     G$     X$     e$     r$     x$     �$     �$  #   �$     �$     �$     �$     %     '%     9%     @%     S%     d%     v%     �%  !   �%     �%     �%     �%     �%  =   �%     &&     A&     _&     d&     }&     �&     �&     �&     �&     �&     �&      '     '     $'     ;'  
   A'     L'  B   Y'     �'  &   �'  %   �'  M   �'     @(     O(     \(     b(     q(  [   �(  &   �(     
)     )  4   ")  F   W)     �)  '   �)     �)     �)  	   �)  0   �)  0   "*     S*     a*     �*  	   �*     �*  X   �*  [   +  g   l+     �+     �+     �+     �+  h   ,     q,  A   y,     �,  >   �,  )   -  9   6-      p-  @   �-  )   �-  )   �-     &.     ).     ..  �  1.  9   0     I0     ]0  
   p0     {0     �0     �0     �0     �0     �0     �0     �0     �0     �0     �0  
   �0     �0     �0     1     1     %1     >1     W1     m1     �1     �1      �1     �1     �1     2     2     ,2     92     S2     g2     v2     �2     �2     �2  �   �2  �   d3  N   4     S4     e4     |4     �4     �4     �4     �4     �4     �4     �4     5     65      V5      w5  *   �5     �5  '   �5  &   6     ,6  +   C6  <   o6  )   �6  )   �6  &    7     '7     @7  ,   P7     }7  1   �7  0   �7  C   �7  U   :8  �   �8  =   B9  6   �9  U   �9  j   :  �   x:     ;  	   );  C   3;  7   w;  	   �;     �;  *   �;     �;     <  ?   *<  +   j<  
   �<     �<  "   �<  ;   �<  B   =  ;   _=     �=     �=  -   �=     �=  H   �=     ;>  
   J>     U>     l>     �>     �>  /   �>  *   �>      ?     ?  8   ?  P   W?     �?     �?     �?     �?     �?     �?     @     @     $@     ?@  4   U@      �@     �@     �@     �@     �@     �@     A  4   A     MA     VA  :   ]A  P   �A  	   �A  \   �A     PB  U   ^B     �B     �B     �B     �B     �B  	   �B     	C  +   C     DC     \C     nC     zC     C     �C      �C  '   �C     �C     D  "   D     ;D     RD     dD     kD     �D     �D     �D     �D  9   �D     E     E     E     .E  I   DE  !   �E  +   �E     �E     �E      F     F     2F     DF     TF     lF     ~F     �F     �F     �F     �F     �F     �F  V   �F     OG  !   ]G     G  [   �G     �G     
H     H     $H     4H  g   NH  ;   �H     �H     I  =   I  _   LI     �I  -   �I     �I     �I     J  8   J  C   LJ     �J     �J     �J  	   �J     �J  H    K  L   IK  [   �K     �K     �K     L     (L  �   >L     �L  E   �L     M  L   0M  3   }M  O   �M     N  V    N  -   wN  4   �N     �N     �N     �N     O   g   �       K   �                           X   �   @   �   �   C       �           !   �   ~       "      -       �       �   &   /   �   �   L          �   x   l   v   I          �       '   �       �   U   6       �   �           e   �   +   �               d   �       1   ,      �      �   �         �   �   P       <   k   �   �      c       �   �   �   �             �   �   V       N           �   �   �   F       :   �   �       �   �       �   A   j   \   {   �       �   �   R   �          �   |       h   �   �   �   G   	       �   a      D              $   �       �   4           �   i   �   �       ?   �   =           �   2   �              �       �       �   ^   �       (   t   �   S   �   `       3   0   z   H   �                �   q   �   Z   �       M       *   �   r   �   5      o       E      �   �   7   �   ;   [   �   _   w           �   �   J   Q   �       �   ]   B       m           �   �   W      �      �       �   >   %   �       �      }   �   .               �      �           �   T   u   p   
               �       �   �   �       �           #   y      �      �   �      �   �       )   �   Y   s   �   �   �   n   �          b   �   �              �               �   �   8                  9       �   f   �       �       �   �                
(c) 2014-{} Juan Pablo Caram

 &Clear Plot &Command Line &Delete &Edit &Exit &File &Help &New &Options &Replot &Save Project &Tool &View &Zoom Fit &Zoom In &Zoom Out 2 (L/R) 2 (T/B) <b>Board cutout:</b> <b>Bounding Box:</b> <b>Create CNC Job:</b> <b>Create CNC Job</b> <b>Export G-Code:</b> <b>Isolation Routing:</b> <b>Mill Holes</b> <b>Non-copper regions:</b> <b>Offset:</b> <b>Paint Area:</b> <b>Plot Options:</b> <b>Scale:</b> <b>Tools</b> APPLICATION DEFAULTS About FlatCAM Add Arc Add Circle Add Path Add Polygon Add Rectangle After clicking here, click inside
the polygon you wish to be painted.
A new Geometry object with the tool
paths will be created. Algorithm to paint the polygon:<BR><B>Standard</B>: Fixed step inwards.<BR><B>Seed-based</B>: Outwards from seed. Amount by which to move the object
in the x and y axes in (x, y) format. Append to G-Code: Application to Object Application to Project Bottom Layer: Boundary Margin: Buffer selection 'b' CNC Job Object CNC Job Options Cancelled. Change the position of this object. Change the size of the object. Click on 1st corner ... Click on 1st point ... Click on 1st point... Click on 2nd point to complete ... Click on CENTER ... Click on a reference point ... Click on a reference point... Click on final location. Click on geometry to select Click on next point or hit SPACE to complete ... Click on opposite corner to complete ... Click on perimeter to complete ... Click on reference point. Click on target point. Combine Passes Combine all passes into one object Copy Objects 'c' Create Geometry for milling holes. Create a CNC Job object
for this drill object. Create a CNC Job object
tracing the contours of this
Geometry object. Create a Geometry object with
toolpaths to cut outside polygons. Create polygons covering the
areas without copper on the PCB.
Equivalent to the inverse of this
object. Can be used to remove all
copper from a specified region. Create the Geometry Object
for isolation routing. Create the Geometry Object
for milling toolpaths. Create toolpaths to cut around
the PCB and separate it from
the original board. Creates a Geometry objects with polygons
covering the copper-free areas of the PCB. Creates tool paths to cover the
whole area of a polygon (remove
all copper). You will be asked
to click on the desired polygon. Cut Path Cut Z: Cutting depth (negative)
below the copper surface. Cutting speed in the XY
plane in units per minute Delete Delete Shape '-' Depth of each pass (positive). Depth/pass: Diameter of the cutting tool. Diameter of the tool to
be used in the operation. Diameter of the tool to be
rendered in the plot. Direction:  Disable all plots Disable all plots but this one Distance by which to avoid
the edges of the polygon to
be painted. Distance from objects at which
to draw the cutout. Distance of the edges of the box
to the nearest polygon. Done. Double-Sided PCB Tool Draw polygons in different colors. Drawing Drill depth (negative)
below the copper surface. Duration [sec.]: Dwell: Edit Geometry Enable all plots Excellon Object Excellon Options Expected a DrawToolShape, got %s Expected a Geometry, got %s Export &SVG ... Export G-Code Export and save G-Code to
make this object to a file. Factor by which to multiply
geometric features of this object. Factor: Feed Rate: Feed rate: FlatCAM Object Gap size: Gaps: Generate Generate Geometry Generate the CNC Job object. Generate the CNC Job. Generate the geometry for
the board cutout. Genrate the Geometry object. Geometry Object Geometry Options Gerber Object Gerber Options Grid X distance Grid Y distante Height of the tool when
moving without cutting. Hello! Home How much (fraction of tool width)
to overlap each pass. How much (fraction) of the tool
width to overlap each tool pass. Idle. If the bounding box is 
to have rounded corners
their radius is equal to
the margin. Import &SVG ... Include tool-change sequence
in G-Code (Pause for tool change). Join Geometry Manual Margin: Max. magnet distance Measurement Tool Method: Mirror Axis: Mirror vertically (X) or horizontally (Y). Move Objects 'm' Multi-Depth: Multicolored Name: New Blank Geometry New Geometry Newer Version Available No active tool to respond to click! Not implemented. Nothing to move. Number of second to dwell. Object to Application Object to Project Offset Open &Excellon ... Open &Gerber ... Open &Project ... Open G-&Code ... Open recent ... Opens dialog to save G-Code
file. Options Overlap: PROJECT OPTIONS Pass overlap: Pause to allow the spindle to reach its
speed before cutting. Perform scaling operation. Perform the offset operation. Plot Plot (show) this object. Polygon Intersection Polygon Subtraction Polygon Union Prepend to G-Code: Project to Application Project to Object Rounded corners Save &Defaults Save Project &As ... Save Project C&opy ... Scale Seed-based Select 'Esc' Select from the tools section above
the tools you want to include. Selected Shape object has empty geometry (None) Shape objects has empty geometry ([]) Size of the gaps in the toolpath
that will remain to hold the
board in place. Snap to corner Snap to grid Solid Solid circles. Solid color polygons. Specify the edge of the PCB
by drawing a box around all
objects with this minimum
distance. Speed of the spindle
in RPM (optional) Spindle speed: Standard The diameter of the cutting
tool (just for display). There is a newer version of FlatCAM available for download:<br><br><B> Tool Tool Z where user can change drill bit
 Tool change Z: Tool change: Tool dia: Tool height when travelling
across the XY plane. Tool speed while drilling
(in units per minute). Toolchange Z: Tools in this Excellon object. Transfer options Travel Z: Type help to get started.

 Type here any G-Code commands you would
like to add at the beginning of the G-Code file. Type here any G-Code commands you would
like to add to the beginning of the generated file. Type here any G-Code commands you would
like to append to the generated file.
I.e.: M2 (End of program) Units: Update Geometry Update Plot Update the plot. Use multiple passes to limit
the cut depth in each pass. Will
cut multiple times until Cut Z is
reached. Vector: Where to place the gaps, Top/Bottom
Left/Rigt, or on all 4 sides. Width (# passes): Width of the isolation gap in
number (integer) of tool widths. Z-axis position (height) for
tool change. [error] Could not parse information about latest version. [success] FlatCAM is up to date! [warning] Failed checking for latest version. Could not connect. [warning] Invalid distance for buffering. [warning] Nothing selected for buffering. cw inch mm Project-Id-Version: FlatCam
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-09-04 10:28+0200
PO-Revision-Date: 2016-09-04 10:29+0200
Last-Translator: Daniel Sallin <info@daniel-stp.fr>
Language-Team: Daniel-STP <info@daniel-stp.fr>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.8
X-Poedit-SourceCharset: UTF-8
X-Poedit-Basepath: ../../..
X-Poedit-KeywordsList: translate_
X-Poedit-SearchPath-0: .
 
(c) 2014-{} Juan Pablo Caram
Translation Daniel Sallin

 &Effacer les traces Ligne de &Commande &Supprimer &Edition &Quitter &Fichier &Aide &Nouveau &Options &Retrace &Sauver projet &Outil &Vue Adapter le &Zoom &Zoom dans &Zoom dehors 2 (G/D) 2 (H/B) <b>Planche découpé:</b> <b>Boite frontière:</b> <b>Créer un job CNC</b> <b>Créer job CNC</b> <b>Exporter le G-Code:</b> <b>Isolation Routage:</b> <b>Trous de fraisage</b> <b>Les régions non-cuivree:</b> <b>Offset:</b> <b>Peinture région</b> <b>Options de trace:</b> <b>Echelle:</b> <b>Outil</b> DEFAUT DE L’APPLICATION A propos de FlatCAM Ajouter un Arc Ajouter un cercle Ajouter un chemin Ajouter un polygone Ajouter un rectangle Après avoir cliqué ici, cliquez à l'intérieur
le polygone que vous souhaitez peindre.
Un nouvel objet de la géométrie avec l'outil
chemins seront créés. Algorithme pour peindre le polygone: <br><b>Standard</b>: Étape fixe vers l'intérieur à <br><b>base de semences.<b>: Vers l'extérieur à partir de graines. Montant par lequel de déplacer l'objet
dans les axes x et y au format (x, y). Ajouter au G-Code Application vers Objet Application vers projet Couche du bas: Marge limites: Sélection de tampon 'b' Objet de Job CNC CNC Options de travail Annuler Change la position de l'objet Change la taille de l'objet. Cliquez sur un premier coin ... Cliquez sur le premier point ... Cliquez sur le premier point ... Cliquer sur un second point pour finir ... Cliquez sur un CENTRE ... Cliquez sur un point de référence ... Cliquez sur un point de référence... Cliquez sur la fin ... Cliquez sur une géométrie pour sélection Cliquer sur le point suivant ou touche ESPACE pour finir ... Cliquez sur un coin oposé pour finir ... Cliquez sur un périmètre pour finir ... Cliquez sur le point de référence... Cliquez sur la cible ... Combinez Passes Mélanger toutes les passes en un seul objet Copier des objets 'c' Créer la géométrie pour les trous de fraisage. Créer un CNC Job objet
pour cet objet de foret. Créer un job d'objet CNC
traçant les contours de cette
Geometrie. Créer un objet de géométrie avec
des parcours à couper les polygones extérieurs. Créer des polygones couvrant la
zones sans cuivre sur le PCB.
Equivalent à l'inverse de cet
objet. Peut être utilisé pour enlever tous les
cuivre d'une région déterminée. Créez la géométrie de l'objet
pour l'isolement de routage. Cree une géométrie d'objet
pour l'outil de fraisage. Créer des parcours pour couper autour
du PCB et le séparer de
la planche d'origine. Crée un des objets géométriques avec des polygones
recouvrir les zones du circuit imprimé sans cuivre. Crée des trajectoires d'outils pour couvrir le
toute la zone d'un polygone (supprimer
tout le cuivre). Il vous sera demandé
cliquer sur le polygone désiré. Couper un chemin Couper Z: Profondeur de coupe (négative)
au-dessous de la surface de cuivre. Vitesse de coupe dans le plan XY
 en unités par minute Supprimer Suprimer une forme '-' Dephasage de recherche de passe (positive) Dephasage/passe: Diamètre de l'outil de coupe Diamètre de l'outil qui peux
être utilisé dans l'opération. Diametre de l'outil à
rendu dans le trace. Direction: Désactiver tout les traces Désactiver tout les autres traces Distance pour éviter
les bords du polygone à
être peint. Distance à partir d'objets à laquelle
pour dessiner la découpe. Distance des bords de la boîte
au polygone le plus proche. Annuler. Outil de PCB recto-verso Afficher les polygone de différentes couleur Dessin Profondeur de percage (négatif) \ n
au-dessous de la surface de cuivre. Durée [sec.]: Lancement: Edition de géométrie Active tous les traces Objet Excellon Options d'Excellon Attendus un outil de dessin en forme, obtenu %s Exeption d'objet de géométrie, obtenu %s Exporter &SVG Exporter G-Code Exporter et enregistrer G-Code à
faire dans un fichier. Facteur par lequel multiplier
les caractéristiques géométriques de cet objet. Facteur: Vitesse d'alimentation: Vitesse d'avance: Objet FlatCAM Taille des écarts Ecarts: Generer Générer une geometrie Genere un jot d'objet CNC. Générer un job CNC. Générer la géométrie pour
la planche découpée. Génère une géométrie d'objet Objet de geometrie Options Géométrie Objet Gerber Options Gerber Distance de grille X Distance de grille Y Hauteur de l'outil quand
se déplaçant sans couper. Bonjour! Maison Combien (fraction d'outil)
pour chevaucher chaque passage. Combien (fraction) de l'outil
largeur pour chevaucher chaque passage de l'outil. Au repos. Si la zone de délimitation est
d'avoir des coins arrondis
leur rayon est égal à
la marge. Importer &SVG Inclure changement de séquence d'outil
en G-Code (Pause pour le changement d'outil). Joindre géometrie Manuel Marge: Distance magnétique maxi Outil de mesure Méthode: Axe du mirroir Miroir vertical (X) ou horizontalement (Y). Déplacer des objet 'm' Multi-Profondeur: Multicolore Nom: Nouvelle géométrie vide Nouvelle Géométrie Version plus récente disponible Pas d'outil actif pour réponde au clic Non implémenter Rien à bouger. Nombre de secondes pour lancement. Objet vers Application Objet vers projet Offset Ouvrir &Excellon ... Ouvrir &Gerber ... Ouvrir &Projet ... Ouvrir G-&Code ... Ouvrir récent ... Ouvrir la boite de dialogue enregistrer
le fichier G-code Options Chevauchement: OPTION DE PROJET Passer chevauchement: Pause pour permettre à la broche d'atteindre sa
vitesse avant de couper. Réalise une opération d'ecalage Réalise une opération de mise a l'echelle Tracé Tracé (visible) de l'objet. Intersection de polygone Soustraction de polygone Union de polygone Avant le G-Code Projet vers Application Projet vers objet Coins arrondis Sauver &Défauts Sauver projet sous ... Sauver projet C&opie ... Echelle Base de semences Sélection 'Esc' Sélectionnez dans la section des outils ci-dessus
les outils que vous voulez inclure. Sélectionner Objet de géométrie vide (Aucun) Objet de géométrie vide ([]) Taille des écarts dans la trajectoire de l'outil
qui restera à tenir le
planche en place. Lâcher le coin Lâcher la grille Continu Cercles pleins. Polygone solid de couleur Indiquez le bord du PCB
en dessinant une boîte autour de tous les
les objets avec ce minimum
distance. La vitesse de la broche
en rotation par minutes (en option) Vitesse de broche: Standard Le diamètre de l'outil de découpe
(juste pour l'affichage). Il existe une version plus récente de FlatCAM disponible pour le téléchargement: <br><br><B> outil Outil Z où l'utilisateur peut changer foret
 Change d'outil Z: Change d'outil: Diamètre outil: Hauteur de l'outil lorsque vous voyagez
dans le plan XY. La vitesse de l'outil en cours de perçage
(En unités par minute). changement d'outil Z Outil dans les objet Excellon. Option de transfert Voyage Z: Voir l'aide pour démarrer.

 Tapez ici toutes commande G-Code
à ajouter au début du fichier G-Code. Tapez ici toute commande G-Code G
à ajouter au début du fichier généré. Tapez ici tout G-Code commande
a ajouter au fichier généré.
I.e .: M2 (Fin du programme) Unites: Mise a jour géométrie Mise à jour du trace Mise à jour du trace Utilisez plusieurs passes pour limiter
la profondeur de coupe à chaque passage. Sera
couper plusieurs fois jusqu'à ce que Cut Z soit
atteint. Vecteur: Où placer les marges, Haut/Bas 
Gauche/Droite, ou sur les 4 côtés. Largeur (# passe): Largeur de l'écart d'isolement dans
Nombre (entier) de la largeurs d'outil. Position de l'axe Z (haut) pour
changement d'outil. [Error] Impossible d'analyser des informations sur la version la plus récente. [Succès] FlatCAM est à jour! [Avertissement] Échec de vérification de dernière version. N'a pas pu se connecter. [Attention] Distance invalide pour le tampon. [Attention] Rien n'est sélectionné pour le tampon. SAM inch mm 